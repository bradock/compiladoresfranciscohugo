lexer grammar DecafLexer;

@header {
package decaf;
}

options
{
  language=Java;
}

TK_class : 'class Program';

LCURLY : '{';
RCURLY : '}';

LPARA : '(';
RPARA : ')';

LCOISA : '[';
RCOISA : ']';

BOO : 'boolean';

BREAK : 'break';

CALLOUT : 'callout';

CLASS : 'class';

CONTINUE : 'continue';

ELSE : 'else';

FALSE : 'false';

FOR : 'for';

INT : 'int';

RETURN : 'return';

TRUE : 'true';

VOID : 'void';

IF: 'if';

SOMA: '+';

SUBTRACAO: '-';

DIVISAO: '/';

MULTIPLICAR: '*';

MODULO: '%';

MAIOR: '>';

MENOR: '<';

DIFERENTE: '!=';

EXCLAN: '!';

ECOMERCIAL: '&&';

IGUAL: '=';

PONTOEVIRGULA : ';';

VIRGULA : ',';

PY: '|';

OULOGICO: '||';

COMPARACAO: '==';

MAIORIGUAL: '>=';

MENORIGUAL: '<=';

MAISIUGUAL: '+=';

MENOSIGUAL: '-='; 

// token com token tem abinguidade

fragment ID  :
  ('a'..'z' | 'A'..'Z')+;

fragment IDHEX : ('a'..'f' | 'A'..'F')+;

WS_ : (' ' | '\n' | '\t') -> skip;

SL_COMMENT : '//' (~'\n')* '\n' -> skip;

CHAR : '\'' (ESC|CARACTER) '\'';

STRING : '"' (ESC|CARACTER)* '"';

IDENTIFICADOR : ('_' | ID) (DIGITS|ID|'_')*;

DIGITS: DIGIT+;

HEXADECI: '0x' (DIGIT|IDHEX)+; 

fragment DIGIT: [0-9];

fragment DIGITNUM: [1-9];

fragment ESC :  '\\' ('n'|'"'|'t'|'\\'|'\'');

fragment CARACTER : (' '|'!'|'#'|'$'|'%'|'&'|'('|')'|'*'|'+'|','|'-'|'.'|'/'|'0'..'9'|':'|';'|'<'|'='|'>'|'?'|'@'|'A'..'Z'|'['|']'|'^'|'_'|'a'..'z'|'{'|'|'|'}'|'~');


